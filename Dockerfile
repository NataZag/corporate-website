# Базовый образ для Python
FROM python:3.11.0

# Установка необходимых зависимостей для chromedriver
RUN apt-get update && apt-get install -y libnss3

# Создаем директорию /app
RUN mkdir -p /app

# Устанавливаем права на выполнение для chromedriver
RUN chmod +x /app/chromedriver

# Копируем файл chromedriver внутрь образа
COPY . .

# Устанавливаем рабочую директорию
WORKDIR /app

# Копируем все файлы из текущей директории внутрь образа
COPY . /app

# Устанавливаем зависимости Python
RUN pip install -r requirements.txt

# Определяем команду запуска приложения
CMD [ "python", "form-test.py" ]
